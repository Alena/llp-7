#include <stdio.h>
#include "mem.h"


int main(void){
    printf("\nNew heap: %llu\n", sizeof(struct mem));

    struct mem* memory_1;
    printf("\nNew heap\n");
    struct mem* memory_2 = _malloc(2000);
    memalloc_debug_heap(stdout,memory_2);

    printf("\nReallocation\n");
    memory_1 = _realloc(memory_2,1000);
    memalloc_debug_heap(stdout,memory_2);

    printf("\nFree\n");
    _free(memory_1);
    memalloc_debug_heap(stdout,memory_2);

    return 0;
}